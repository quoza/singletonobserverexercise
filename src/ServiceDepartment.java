import java.io.*;

class ServiceDepartment {
    private static int serviceDepartmentID = Request.getRequestID();

    ServiceDepartment(Request request) throws IOException {
        if (request.getRequestType().equals(RequestType.SERVICE_REQUEST)) {
            BufferedWriter bw = new BufferedWriter(new FileWriter("usersdb.txt"));
            bw.append(String.valueOf(Request.getRequestID())).append(",").append(request.getRequestType().toString())
                    .append(",").append(request.getRequestTitle()).append(",").append(request.getRequestContent())
                    .append(",").append(request.getRequester());
            bw.newLine();
            bw.flush();
            bw.close();
            System.out.println(Request.getRequestID() + "," + request.getRequestType().toString() + "," +
                    request.getRequestTitle() + "," + request.getRequestContent());
        } else {
            throw new RuntimeException("Wrong request type!");
        }
    }

    public static int getServiceDepartmentID() {
        return serviceDepartmentID;
    }

    public static void setServiceDepartmentID(int serviceDepartmentID) {
        ServiceDepartment.serviceDepartmentID = serviceDepartmentID;
    }
}