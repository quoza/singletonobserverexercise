import java.io.*;
import java.util.*;

public class Database {

    public void saveToDatabase(DatabaseName dbName, Object obj){
        saveToDatabase(dbName.getDbType(),obj);
    }

    public void saveToDatabase(DatabaseName dbName, Request request){
        saveToDatabase(dbName.getDbType(), request);
    }

    public void saveToDatabase(String dbName, Object obj){
        try(PrintWriter writer = new PrintWriter(new FileOutputStream(dbName,true))){
            writer.println(obj);
        } catch (FileNotFoundException fnfe){
            System.err.println("File not found!");
        }
    }

    public void appendServiceRequest(String dbName, Request request){
        String userName = request.getRequester();
        String content = request.getRequestContent();

        Map<String, List<String>> fileContent = readFile(dbName);

        List<String> list;
        if (fileContent.containsKey(userName)) {
            list = fileContent.get(userName);
            list.add(content);
        } else {
            list = new LinkedList<>();
            list.add(content);
        }
        fileContent.put(userName, list);
        System.out.println("Service record added to the list.");
        saveRecordsToFile(dbName,fileContent);
    }

    private void saveRecordsToFile(String dbName, Map<String, List<String>> fileContent) {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(dbName, true))) {
            for (Map.Entry<String, List<String>> entry : fileContent.entrySet()) {
                StringBuilder sb = new StringBuilder();
                sb.append(entry.getKey());
                sb.append(";");

                for (String content : entry.getValue()) {
                    sb.append(content);
                    sb.append(":");
                }
                writer.println(sb.substring(0, sb.length() - 1));
            }

        } catch (FileNotFoundException fnfe) {
            System.err.println("Error reading file.");
        }
        System.out.println("File " + dbName + " has been saved.");
    }

    private void saveRecordsToFileOther(String dbName, Map<String, List<String>> fileContent) {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(dbName, true))) {
            for (Map.Entry<String, List<String>> entry : fileContent.entrySet()) {
                for (String content : entry.getValue()) {
                    writer.println(entry.getKey() + ";" + content);
                }
            }

        } catch (FileNotFoundException fnfe) {
            System.err.println("Error reading file.");
        }
        System.out.println("File " + dbName + " has been saved.");
    }

    private Map<String, List<String>> readFile(String fileName) {
        Map<String, List<String>> dataMap = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                String[] lineSplitted = line.split(",");
                String userName = lineSplitted[0];
                String[] contents = lineSplitted[1].split(":");
                List<String> list = new LinkedList<>(Arrays.asList(contents));
                dataMap.put(userName, list);
            }
        } catch (FileNotFoundException fnfe) {
            System.err.println("File not found!");
        } catch (IOException ioe) {
            System.err.println("Input/Output exception!");
        }
        return dataMap;
    }

    private Map<String, List<String>> readFileOther(String fileName) {
        Map<String, List<String>> dataMap = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = null;
            while ((line = br.readLine()) != null) {
                String[] lineSplitted = line.split(",");
                String userName = lineSplitted[0];
                String content = lineSplitted[1];

                if (!dataMap.containsKey(userName)) {
                    dataMap.put(userName, new LinkedList<>());
                } else {
                    dataMap.get(userName).add(content);
                }
            }
        } catch (IOException e) {
            System.out.println("Input/Output Exception!");
        }
        return dataMap;
    }
}