import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

class Order {
    private static int orderID = Request.getRequestID();

    Order(Request request) throws IOException {
        if (request.getRequestType().equals(RequestType.HARDWARE_REQUEST)) {
            BufferedWriter bw = new BufferedWriter(new FileWriter("requestsdb.txt", true));
            bw.append(String.valueOf(Request.getRequestID())).append(",").append(request.getRequestType().toString())
                    .append(",").append(request.getRequestTitle()).append(",").append(request.getRequestContent())
                    .append(",").append(request.getRequester());
            bw.newLine();
            bw.flush();
            bw.close();
            System.out.println(Request.getRequestID() + "," + request.getRequestType().toString() + "," +
                    request.getRequestTitle() + "," + request.getRequestContent());
        } else {
            throw new RuntimeException("Wrong request type!");
        }
    }

    public static int getOrderID() {
        return orderID;
    }

    public static void setOrderID(int orderID) {
        Order.orderID = orderID;
    }
}
