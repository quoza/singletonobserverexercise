public enum DatabaseName {
    DB_USERS("usersdb.txt"),DB_ORDER("ordersdb.txt"),DB_REQUESTS("requestsdb.txt");

    private String dbType;

    DatabaseName(String dbType){
        this.dbType = dbType;
    }

    public String getDbType() {
        return dbType;
    }
}
