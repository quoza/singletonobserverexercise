class FinanceDepartment {
    private static FinanceDepartment instance;

    private FinanceDepartment() {

    }

    public static FinanceDepartment getInstance(){
        return FinanceDepartmentHolder.INSTANCE;
    }

    private static class FinanceDepartmentHolder {
        private static final FinanceDepartment INSTANCE = new FinanceDepartment();
    }
}