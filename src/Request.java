import java.io.IOException;

public class Request {
    private static int requestID;
    private String requestTitle;
    private String requestContent;
    private RequestType requestType;
    private String requester;

    public Request(String requestTitle, String requestContent, RequestType requestType, String requester) {
        requestID++;
        this.requestTitle = requestTitle;
        this.requestContent = requestContent;
        this.requestType = requestType;
        this.requester = requester;
        if (requestType.equals(RequestType.HARDWARE_REQUEST)) {
            try {
                new Order(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (requestType.equals(RequestType.HARDWARE_REQUEST)) {
            try {
                new ServiceDepartment(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static int getRequestID() {
        return requestID;
    }

    public static void setRequestID(int requestID) {
        Request.requestID = requestID;
    }

    public String getRequestTitle() {
        return requestTitle;
    }

    public void setRequestTitle(String requestTitle) {
        this.requestTitle = requestTitle;
    }

    public String getRequestContent() {
        return requestContent;
    }

    public void setRequestContent(String requestContent) {
        this.requestContent = requestContent;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }
}
